import pytest

ENDPOINT = '/api/contracts/'

from utils.subgroup_util import Subgroup

@pytest.mark.django_db
class TestContractEndpoint:
    def setup_method(self):
        self.contract_test_supply_voltage_1 = -1 # CT1
        self.contract_test_supply_voltage_2 = 69 # CT2
        self.contract_test_supply_voltage_3 = 230  # CT3
        self.contract_test_supply_voltage_4 = 70  # CT4

    def test_throws_exception_when_suply_voltage_below_minimum(self):
        with pytest.raises(Exception) as e:
            Subgroup.get_subgroup(self.contract_test_supply_voltage_1)
        assert 'Subgroup not found' in str(e.value)

    def test_get_group_minimum_and_maximum_equal(self):
        assert Subgroup.get_subgroup(self.contract_test_supply_voltage_2) == Subgroup.A3

    def test_get_group_without_maximum_voltage(self):
        assert Subgroup.get_subgroup(self.contract_test_supply_voltage_3) == Subgroup.A1

    def test_throws_exception_when_suply_voltage_does_not_match_ranges(self):
        with pytest.raises(Exception) as e:
            Subgroup.get_subgroup(self.contract_test_supply_voltage_4)
        assert 'Subgroup not found' in str(e.value)